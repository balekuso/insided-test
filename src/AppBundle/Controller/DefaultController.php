<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use AppBundle\Form\PostType; 
use AppBundle\Entity\Post;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
     public function indexAction(Request $request)
    {
        $errors =""; 
        //check if we have post stored and keep the last one on the top
         $posts = $this->getDoctrine()
                ->getRepository('AppBundle:Post')
                ->findBy([], ['id' => 'DESC']);

        if (!$posts) {
            $posts = null;     
        }

         
        // create a post form to show in the page
        $post = new Post();
        $post->setTitle('');
        $form = $this->createFormBuilder($post)
            ->add('title', TextType::class, array('attr' => array('placeholder' => 'write a Title','class' => 'border-form'),
                                                  'required' => false,
                                                  'empty_data'  => "")
                 )
            ->add('filename', FileType::class)
            ->add('save', SubmitType::class, array('label' => 'Create Post'))
            ->getForm();
         
        //if we get a request, because the user send a new post
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // $file stores the uploaded file
            /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
            $file = $post->getfilename();
            
            $extension = $file->guessExtension();
            $valid_extension = array ("jpg","jpeg","gif","png");
            //just we keep jpg, gif and png, others will be dismissed
            if (in_array($extension, $valid_extension)){

                // Generate a unique name for the file before saving it
                $fileName = md5(uniqid()).'.'.$file->guessExtension();

                $em = $this->getDoctrine()->getManager();
                $post->setfilename($fileName);
                $em->persist($post);
                $em->flush();

                // Move the file to the directory where images are stored
                $file->move(
                    $this->getParameter('images_directory'),
                    $fileName
                );

                // ... persist the $product variable or any other work
                //if we stored a new one we get the new posts list
                 $posts = $this->getDoctrine()
                        ->getRepository('AppBundle:Post')
                         ->findBy([], ['id' => 'DESC']);

                 if (!$posts) {
                     $posts = null;     
                 }
                
                

                return $this->render('main/index.html.twig', array(
                'form' => $form->createView(),
                'posts' => $posts, 
                'errors' => $errors,
                    
                ));
                
            }else{
                //the file type is not a valid picturo so we send a error 
                $errors ="file should be jpg,gif,png";
                return $this->render('main/index.html.twig', array(
                'form' => $form->createView(),
                'posts' => $posts, 
                'errors' => $errors,
                ));
            } 
        }
         
       
        return $this->render('main/index.html.twig', array(
            'form' => $form->createView(),
            'posts' =>$posts,
            'errors' => $errors,
        ));
    }
}
