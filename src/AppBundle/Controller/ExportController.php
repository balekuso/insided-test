<?php
namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\StreamedResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\PostType;
use AppBundle\Entity\Post;

class ExportController extends Controller
{
    /**
     * 
     *
     * @Route("/export", name="export_route_name")
     * @return RedirectResponse
     *
     */
    public function generateCsvAction()
    {
       // This method will contain the CSV exporting logic
        $response = new StreamedResponse();
        $response->setCallback(function() {
            $handle = fopen('php://output', 'w+');

            // Add the header of the CSV file
            fputcsv($handle, array('title', 'filename'),' ');
            
            $posts = $this->getDoctrine()
                ->getRepository('AppBundle:Post')
                ->findAll();

                if (!$posts) {
                    $posts = null;     
                }

                foreach( $posts as $post){
                    fputcsv(
                    $handle, // The file pointer
                    array($post->getTitle(), $post->getFilename()), // The fields
                    ' ' // The delimiter
                  );
                }
            
            fclose($handle);
        });

    $response->setStatusCode(200);
    $response->headers->set('Content-Type', 'text/csv; charset=utf-8');
    $response->headers->set('Content-Disposition', 'attachment; filename="export.csv"');

    return $response;
    }
}