<?php

/* @Twig/Exception/exception.json.twig */
class __TwigTemplate_4e793921cce2aa7bec3929ba16ceb0c77ec5a1a0c2455ea3dcc59ddc4f6f8b28 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ad0b7fae30985120dd840e8f2f4d75c7efb1dfca5a7bffaf643a97286d831000 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ad0b7fae30985120dd840e8f2f4d75c7efb1dfca5a7bffaf643a97286d831000->enter($__internal_ad0b7fae30985120dd840e8f2f4d75c7efb1dfca5a7bffaf643a97286d831000_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.json.twig"));

        // line 1
        echo twig_jsonencode_filter(array("error" => array("code" => (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "message" => (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "exception" => $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "toarray", array()))));
        echo "
";
        
        $__internal_ad0b7fae30985120dd840e8f2f4d75c7efb1dfca5a7bffaf643a97286d831000->leave($__internal_ad0b7fae30985120dd840e8f2f4d75c7efb1dfca5a7bffaf643a97286d831000_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSource()
    {
        return "{{ { 'error': { 'code': status_code, 'message': status_text, 'exception': exception.toarray } }|json_encode|raw }}
";
    }
}
