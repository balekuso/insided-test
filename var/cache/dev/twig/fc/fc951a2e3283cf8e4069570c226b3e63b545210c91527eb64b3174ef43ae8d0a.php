<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_a35066dae42fb40c074583893bb2924fe94107ca733a7885915cce9d65b80ee3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_053362ed02058b323e3046ea848527c79507b99117dd78218e99af789120a52b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_053362ed02058b323e3046ea848527c79507b99117dd78218e99af789120a52b->enter($__internal_053362ed02058b323e3046ea848527c79507b99117dd78218e99af789120a52b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_053362ed02058b323e3046ea848527c79507b99117dd78218e99af789120a52b->leave($__internal_053362ed02058b323e3046ea848527c79507b99117dd78218e99af789120a52b_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_9f7345851183d6f43ec90183a6e2456eac5a82b390ca29e2affd522eedb3bd86 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9f7345851183d6f43ec90183a6e2456eac5a82b390ca29e2affd522eedb3bd86->enter($__internal_9f7345851183d6f43ec90183a6e2456eac5a82b390ca29e2affd522eedb3bd86_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_9f7345851183d6f43ec90183a6e2456eac5a82b390ca29e2affd522eedb3bd86->leave($__internal_9f7345851183d6f43ec90183a6e2456eac5a82b390ca29e2affd522eedb3bd86_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_cca872411c9deb506e9bf42dba32ab188249c9348273f30204f19904ff79f644 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cca872411c9deb506e9bf42dba32ab188249c9348273f30204f19904ff79f644->enter($__internal_cca872411c9deb506e9bf42dba32ab188249c9348273f30204f19904ff79f644_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_cca872411c9deb506e9bf42dba32ab188249c9348273f30204f19904ff79f644->leave($__internal_cca872411c9deb506e9bf42dba32ab188249c9348273f30204f19904ff79f644_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_0e6f61bd789b5b1bfa1546340895e19057f76a7760630c10d837aabe80d221f8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0e6f61bd789b5b1bfa1546340895e19057f76a7760630c10d837aabe80d221f8->enter($__internal_0e6f61bd789b5b1bfa1546340895e19057f76a7760630c10d837aabe80d221f8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\HttpKernelExtension')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_0e6f61bd789b5b1bfa1546340895e19057f76a7760630c10d837aabe80d221f8->leave($__internal_0e6f61bd789b5b1bfa1546340895e19057f76a7760630c10d837aabe80d221f8_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }

    public function getSource()
    {
        return "{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
";
    }
}
