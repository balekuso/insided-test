<?php

/* base.html.twig */
class __TwigTemplate_040bec09f39b6a5e46b5cf093ae3d8e270138bc70c91349cffd7f8d0ed0ada6a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7c417d35e7f8cdbf267c37a98ec272cbc22cfcc49b57defc13487be0d7d1302c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7c417d35e7f8cdbf267c37a98ec272cbc22cfcc49b57defc13487be0d7d1302c->enter($__internal_7c417d35e7f8cdbf267c37a98ec272cbc22cfcc49b57defc13487be0d7d1302c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        
        <link type=\"text/css\" rel=\"stylesheet\" media=\"all\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/framework/css/bootstrap.css"), "html", null, true);
        echo "\" />
        <link type=\"text/css\" rel=\"stylesheet\" media=\"all\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/framework/css/body.css"), "html", null, true);
        echo "\" />
        <link href=\"https://fonts.googleapis.com/css?family=Indie+Flower\" rel=\"stylesheet\">

        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 14
        $this->displayBlock('body', $context, $blocks);
        // line 15
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 22
        echo "    </body>
</html>
";
        
        $__internal_7c417d35e7f8cdbf267c37a98ec272cbc22cfcc49b57defc13487be0d7d1302c->leave($__internal_7c417d35e7f8cdbf267c37a98ec272cbc22cfcc49b57defc13487be0d7d1302c_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_c557230d771f719c20accf80c6318600b632a35e25f1d0d708e047f84359ca04 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c557230d771f719c20accf80c6318600b632a35e25f1d0d708e047f84359ca04->enter($__internal_c557230d771f719c20accf80c6318600b632a35e25f1d0d708e047f84359ca04_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_c557230d771f719c20accf80c6318600b632a35e25f1d0d708e047f84359ca04->leave($__internal_c557230d771f719c20accf80c6318600b632a35e25f1d0d708e047f84359ca04_prof);

    }

    // line 14
    public function block_body($context, array $blocks = array())
    {
        $__internal_1dbd8e8c7e65cf575f9ff7eefc1ab7070bf8fde4fc919e7c9ef56c4b9a936f54 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1dbd8e8c7e65cf575f9ff7eefc1ab7070bf8fde4fc919e7c9ef56c4b9a936f54->enter($__internal_1dbd8e8c7e65cf575f9ff7eefc1ab7070bf8fde4fc919e7c9ef56c4b9a936f54_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_1dbd8e8c7e65cf575f9ff7eefc1ab7070bf8fde4fc919e7c9ef56c4b9a936f54->leave($__internal_1dbd8e8c7e65cf575f9ff7eefc1ab7070bf8fde4fc919e7c9ef56c4b9a936f54_prof);

    }

    // line 15
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_ed0a64c9acd93881f758e2b7370c7b247fd116634f7ff2eed509eca7e5d1034b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ed0a64c9acd93881f758e2b7370c7b247fd116634f7ff2eed509eca7e5d1034b->enter($__internal_ed0a64c9acd93881f758e2b7370c7b247fd116634f7ff2eed509eca7e5d1034b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 16
        echo "            
            <script src=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/framework/js/jquery.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/framework/js/bootstrap-3.3.4.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/framework/js/app.js"), "html", null, true);
        echo "\"></script>

        ";
        
        $__internal_ed0a64c9acd93881f758e2b7370c7b247fd116634f7ff2eed509eca7e5d1034b->leave($__internal_ed0a64c9acd93881f758e2b7370c7b247fd116634f7ff2eed509eca7e5d1034b_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  106 => 19,  102 => 18,  98 => 17,  95 => 16,  89 => 15,  78 => 14,  66 => 5,  57 => 22,  54 => 15,  52 => 14,  46 => 11,  40 => 8,  36 => 7,  31 => 5,  25 => 1,);
    }

    public function getSource()
    {
        return "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Welcome!{% endblock %}</title>
        
        <link type=\"text/css\" rel=\"stylesheet\" media=\"all\" href=\"{{ asset('bundles/framework/css/bootstrap.css') }}\" />
        <link type=\"text/css\" rel=\"stylesheet\" media=\"all\" href=\"{{ asset('bundles/framework/css/body.css') }}\" />
        <link href=\"https://fonts.googleapis.com/css?family=Indie+Flower\" rel=\"stylesheet\">

        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
    </head>
    <body>
        {% block body %}{% endblock %}
        {% block javascripts %}
            
            <script src=\"{{ asset('bundles/framework/js/jquery.js') }}\"></script>
            <script src=\"{{ asset('bundles/framework/js/bootstrap-3.3.4.js') }}\"></script>
            <script src=\"{{ asset('bundles/framework/js/app.js') }}\"></script>

        {% endblock %}
    </body>
</html>
";
    }
}
