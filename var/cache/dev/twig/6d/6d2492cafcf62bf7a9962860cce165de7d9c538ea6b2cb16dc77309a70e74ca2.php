<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_c0f238739f202c3d256685f0406f89c9811b1f5c0f890f747f524f9469d1fba6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ef00b3241f2cecf422a56494e3ae67b3ebdfc96e8fe632e4d9b721a7cec29efd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ef00b3241f2cecf422a56494e3ae67b3ebdfc96e8fe632e4d9b721a7cec29efd->enter($__internal_ef00b3241f2cecf422a56494e3ae67b3ebdfc96e8fe632e4d9b721a7cec29efd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ef00b3241f2cecf422a56494e3ae67b3ebdfc96e8fe632e4d9b721a7cec29efd->leave($__internal_ef00b3241f2cecf422a56494e3ae67b3ebdfc96e8fe632e4d9b721a7cec29efd_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_7d93ca2b833fa1a91050d4f1a05b79add68483e6a213d8224d38acb3161bf48e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7d93ca2b833fa1a91050d4f1a05b79add68483e6a213d8224d38acb3161bf48e->enter($__internal_7d93ca2b833fa1a91050d4f1a05b79add68483e6a213d8224d38acb3161bf48e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\HttpFoundationExtension')->generateAbsoluteUrl($this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_7d93ca2b833fa1a91050d4f1a05b79add68483e6a213d8224d38acb3161bf48e->leave($__internal_7d93ca2b833fa1a91050d4f1a05b79add68483e6a213d8224d38acb3161bf48e_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_0d4fee285f4649bcf5a030b9e3819f39326b78a1b80d3c64c2b5d15c1fb97977 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0d4fee285f4649bcf5a030b9e3819f39326b78a1b80d3c64c2b5d15c1fb97977->enter($__internal_0d4fee285f4649bcf5a030b9e3819f39326b78a1b80d3c64c2b5d15c1fb97977_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_0d4fee285f4649bcf5a030b9e3819f39326b78a1b80d3c64c2b5d15c1fb97977->leave($__internal_0d4fee285f4649bcf5a030b9e3819f39326b78a1b80d3c64c2b5d15c1fb97977_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_2113ccaae541cede0ad121c0dea1ce9464b6d983aad92e3acf63ce32fac113f0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2113ccaae541cede0ad121c0dea1ce9464b6d983aad92e3acf63ce32fac113f0->enter($__internal_2113ccaae541cede0ad121c0dea1ce9464b6d983aad92e3acf63ce32fac113f0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 12)->display($context);
        
        $__internal_2113ccaae541cede0ad121c0dea1ce9464b6d983aad92e3acf63ce32fac113f0->leave($__internal_2113ccaae541cede0ad121c0dea1ce9464b6d983aad92e3acf63ce32fac113f0_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }

    public function getSource()
    {
        return "{% extends '@Twig/layout.html.twig' %}

{% block head %}
    <link href=\"{{ absolute_url(asset('bundles/framework/css/exception.css')) }}\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ status_code }} {{ status_text }})
{% endblock %}

{% block body %}
    {% include '@Twig/Exception/exception.html.twig' %}
{% endblock %}
";
    }
}
