<?php

/* main/index.html.twig */
class __TwigTemplate_936007395ad9c110ca4dfc2c9ff1b95b1b5d9b4b8513c666ece95adc7296cba7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "main/index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_12f0584282359f7b8a86e7044c06517399b70b1f51b96a03d99cfb5740b70aba = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_12f0584282359f7b8a86e7044c06517399b70b1f51b96a03d99cfb5740b70aba->enter($__internal_12f0584282359f7b8a86e7044c06517399b70b1f51b96a03d99cfb5740b70aba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "main/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_12f0584282359f7b8a86e7044c06517399b70b1f51b96a03d99cfb5740b70aba->leave($__internal_12f0584282359f7b8a86e7044c06517399b70b1f51b96a03d99cfb5740b70aba_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_9c542600da82a94b9f6207c384266f2c628fcb7cf8345b971fd88fd67bc6bd72 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9c542600da82a94b9f6207c384266f2c628fcb7cf8345b971fd88fd67bc6bd72->enter($__internal_9c542600da82a94b9f6207c384266f2c628fcb7cf8345b971fd88fd67bc6bd72_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
   <div class=\"margin-top60\">
       <div class=\"col-sm-offset-1 col-sm-3\">
            <h1>POSTS: <span id=\"posts\">";
        // line 7
        echo twig_escape_filter($this->env, twig_length_filter($this->env, (isset($context["posts"]) ? $context["posts"] : $this->getContext($context, "posts"))), "html", null, true);
        echo "</span></h1>
       </div>

       <div class=\"col-sm-3\">
         <a class=\"btn-web\" href=\"";
        // line 11
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("export_route_name");
        echo "\">Export CVS</a>
       </div>  

        <div class=\"col-sm-offset-1 col-sm-3\">
            <h1>VIEWS: <span id=\"views\">0</span></h1>
       </div>
   </div>   
   <hr>
   <div class=\"col-sm-offset-1 col-sm-10 margin-top60\">
       <div class=\"btn-post\">
        ";
        // line 21
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
            ";
        // line 23
        echo "            ";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "title", array()), 'row');
        echo "
            ";
        // line 24
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "filename", array()), 'row');
        echo "
        ";
        // line 25
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
        </div>   
       <div class=\"error\">";
        // line 27
        echo twig_escape_filter($this->env, (isset($context["errors"]) ? $context["errors"] : $this->getContext($context, "errors")), "html", null, true);
        echo "</div>
   </div>  

   <div class=\"col-sm-offset-3 col-sm-5\">
     <ul>
         <div class=\"list\">
         ";
        // line 33
        if ( !twig_test_empty(((array_key_exists("posts", $context)) ? (_twig_default_filter((isset($context["posts"]) ? $context["posts"] : $this->getContext($context, "posts")))) : ("")))) {
            // line 34
            echo "            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["posts"]) ? $context["posts"] : $this->getContext($context, "posts")));
            foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
                // line 35
                echo "               
                    <li>
                        <a class=\"title\" target=\"_blank\" href=\"api/posts/";
                // line 37
                echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "id", array()), "html", null, true);
                echo "\"><h1>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "title", array()), "html", null, true);
                echo "</h1>
                        <img class=\"img-responsive\" src=\"uploads/images/";
                // line 38
                echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "filename", array()), "html", null, true);
                echo "\" /></a>
                    </li>    
                    <hr>
             
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 43
            echo "          ";
        }
        // line 44
        echo "         </div>  
     </ul>
   </div>

";
        
        $__internal_9c542600da82a94b9f6207c384266f2c628fcb7cf8345b971fd88fd67bc6bd72->leave($__internal_9c542600da82a94b9f6207c384266f2c628fcb7cf8345b971fd88fd67bc6bd72_prof);

    }

    public function getTemplateName()
    {
        return "main/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  123 => 44,  120 => 43,  109 => 38,  103 => 37,  99 => 35,  94 => 34,  92 => 33,  83 => 27,  78 => 25,  74 => 24,  69 => 23,  65 => 21,  52 => 11,  45 => 7,  40 => 4,  34 => 3,  11 => 1,);
    }

    public function getSource()
    {
        return "{% extends 'base.html.twig' %}
  
{% block body %}

   <div class=\"margin-top60\">
       <div class=\"col-sm-offset-1 col-sm-3\">
            <h1>POSTS: <span id=\"posts\">{{ posts|length}}</span></h1>
       </div>

       <div class=\"col-sm-3\">
         <a class=\"btn-web\" href=\"{{ path('export_route_name') }}\">Export CVS</a>
       </div>  

        <div class=\"col-sm-offset-1 col-sm-3\">
            <h1>VIEWS: <span id=\"views\">0</span></h1>
       </div>
   </div>   
   <hr>
   <div class=\"col-sm-offset-1 col-sm-10 margin-top60\">
       <div class=\"btn-post\">
        {{ form_start(form) }}
            {# ... #}
            {{ form_row(form.title) }}
            {{ form_row(form.filename) }}
        {{ form_end(form) }}
        </div>   
       <div class=\"error\">{{ errors }}</div>
   </div>  

   <div class=\"col-sm-offset-3 col-sm-5\">
     <ul>
         <div class=\"list\">
         {% if posts|default is not empty %}
            {% for post in posts %}
               
                    <li>
                        <a class=\"title\" target=\"_blank\" href=\"api/posts/{{ post.id }}\"><h1>{{ post.title }}</h1>
                        <img class=\"img-responsive\" src=\"uploads/images/{{ post.filename }}\" /></a>
                    </li>    
                    <hr>
             
            {% endfor %}
          {% endif %}
         </div>  
     </ul>
   </div>

{% endblock %}";
    }
}
